<?php

declare(strict_types=1);

namespace App\Command;

use App\Entity\User;
use App\Service\FirestoreService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Mailer\MailerInterface;

class EmailUsersCommand extends Command
{
    /**
     * @var string
     */
    protected static $defaultName = 'email:users';

    /**
     * @var FirestoreService
     */
    private $firestore;

    /**
     * @var MailerInterface
     */
    private $mailer;

    /**
     * @var EntityManagerInterface
     */
    private $manager;

    /**
     * @var string
     */
    private $mailerSender;

    /**
     * @param FirestoreService $firestore
     * @param MailerInterface $mailer
     * @param EntityManagerInterface $manager
     * @param string $mailerSender
     * @param string|null $name
     */
    public function __construct(
        FirestoreService $firestore,
        MailerInterface $mailer,
        EntityManagerInterface $manager,
        string $mailerSender,
        string $name = null
    ) {
        $this->firestore = $firestore;
        $this->mailer = $mailer;
        $this->manager = $manager;
        $this->mailerSender = $mailerSender;

        parent::__construct($name);
    }

    /**
     * @inheritDoc
     */
    protected function configure()
    {
        $this->setDescription('Send emails to all users with unread messages');
    }

    /**
     * @inheritDoc
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        /** @var ProgressBar $progressBar */
        $progressBar = $io->createProgressBar(100);
        $progressBar->display();
        $progressBar->start();
        $unreadMessages = $this->firestore->unreadMessagesMoreThan15Minutes();
        $progressBar->advance(20);
        $groupedMessages = $this->groupMessagesByReceiver($unreadMessages);
        $progressBar->advance(15);
        $io->writeln('');

        foreach ($groupedMessages as $userId => $data)
        {
            /** @var User $userTo */
            $userTo = $this->manager->getRepository(User::class)->find($userId);

            foreach ($data as $senderId => $messages)
            {
                /** @var User $userFrom */
                $userFrom = $this->manager->getRepository(User::class)->find($senderId);
                $email = $this->emailTemplate($userTo, $userFrom, $messages);
                $this->mailer->send($email);
                $io->writeln(
                    'Email send to '. $userTo->getEmail().
                    ' notified for messages from '. $userFrom->getEmail()
                );
            }
        }

        $progressBar->advance(65);
        $progressBar->finish();
        $io->success('Emails have been send successfully to all users.');

        return Command::SUCCESS;
    }

    /**
     * @param array $messages
     *
     * @return array
     */
    private function groupMessagesByReceiver(array $messages)
    {
        $groupedMessages = [];
        foreach ($messages as $message)
            $groupedMessages[$message['receiver']][$message['sender']][] = $message;

        return $groupedMessages;
    }

    /**
     * Returns an Email Template
     *
     * @param User $userTo
     * @param User $userFrom
     * @param array $messages
     *
     * @return TemplatedEmail
     */
    private function emailTemplate(User $userTo, User $userFrom, array $messages)
    {
        return (new TemplatedEmail())
            ->from($this->mailerSender)
            ->to($userTo->getEmail())
            ->subject('Unread Messages')
            ->htmlTemplate('email/unread_messages.html.twig')
            ->context([
                'to' => $userTo,
                'from' => $userFrom,
                'messages' => $messages
            ]);
    }
}
