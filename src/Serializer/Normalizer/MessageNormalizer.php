<?php

declare(strict_types=1);

namespace App\Serializer\Normalizer;

use App\Entity\User;
use App\Firestore\Message;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Serializer\Normalizer\CacheableSupportsMethodInterface;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

class MessageNormalizer implements NormalizerInterface, DenormalizerInterface, CacheableSupportsMethodInterface
{
    /**
     * @var ObjectNormalizer
     */
    private $normalizer;

    /**
     * @var EntityManagerInterface
     */
    private $manager;

    /**
     * @param ObjectNormalizer $normalizer
     * @param EntityManagerInterface $manager
     */
    public function __construct(ObjectNormalizer $normalizer, EntityManagerInterface $manager)
    {
        $this->normalizer = $normalizer;
        $this->manager = $manager;
    }

    /**
     * @inheritDoc
     */
    public function normalize($object, string $format = null, array $context = [])
    {
        $data = $this->normalizer->normalize($object, $format, $context);
        $data['sender'] = $data['sender']['id'];
        $data['receiver'] = $data['receiver']['id'];
        $data['sendAt'] = $object->getSendAt()->getTimestamp();

        return $data;
    }

    /**
     * @inheritDoc
     */
    public function supportsNormalization($data, string $format = null)
    {
        return $data instanceof Message;
    }

    /**
     * @inheritDoc
     */
    public function denormalize($data, string $type, string $format = null, array $context = [])
    {
        $receiver = $this->manager->getRepository(User::class)->find($data['receiver']);
        $sender = $this->manager->getRepository(User::class)->find($data['sender']);
        $data['sendAt'] = (new DateTime)->setTimestamp($data['sendAt'])->format(DateTime::ATOM);

        /** @var Message $message */
        $message = $this->normalizer->denormalize($data, $type, $format, $context);
        $message->setReceiver($receiver);
        $message->setSender($sender);

        return $message;
    }

    /**
     * @inheritDoc
     */
    public function supportsDenormalization($data, string $type, string $format = null)
    {
        return Message::class === $type;
    }

    /**
     * @inheritDoc
     */
    public function hasCacheableSupportsMethod(): bool
    {
        return true;
    }
}
