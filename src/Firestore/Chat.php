<?php

declare(strict_types=1);

namespace App\Firestore;

use App\Entity\User;

class Chat
{
    /**
     * @var string
     */
    private $id;

    /**
     * @var User
     */
    private $you;

    /**
     * @var User
     */
    private $otherUser;

    /**
     * @param User $you
     * @param User $otherUser
     *
     * @return Chat
     */
    public static function withDefaultValues(User $you, User $otherUser) : self
    {
        $chat = new self();
        $usersIds = [$you->getId(), $otherUser->getId()];
        sort($usersIds);
        $chat->setId('chat_' . implode('_', $usersIds));
        $chat->setYou($you);
        $chat->setOtherUser($otherUser);

        return $chat;
    }

    /**
     * @return string
     */
    public function getId() {
        return $this->id;
    }

    /**
     * @param string $id
     */
    private function setId(string $id) : void
    {
        $this->id = $id;
    }

    /**
     * @return User
     */
    public function getYou(): User
    {
        return $this->you;
    }

    /**
     * @param User $you
     */
    private function setYou(User $you): void
    {
        $this->you = $you;
    }

    /**
     * @return User
     */
    public function getOtherUser(): User
    {
        return $this->otherUser;
    }

    /**
     * @param User $otherUser
     */
    private function setOtherUser(User $otherUser): void
    {
        $this->otherUser = $otherUser;
    }
}
