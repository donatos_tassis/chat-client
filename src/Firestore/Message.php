<?php

declare(strict_types=1);

namespace App\Firestore;

use App\Entity\User;
use DateTime;

class Message
{
    /**
     * @var string
     */
    private $id;

    /**
     * @var string
     */
    private $chatId;

    /**
     * @var User
     */
    private $sender;

    /**
     * @var User
     */
    private $receiver;

    /**
     * @var string
     */
    private $text;

    /**
     * @var DateTime
     */
    private $send_at;

    /**
     * @var bool
     */
    private $isDeleted;

    /**
     * @var bool
     */
    private $isRead;

    /**
     * Constructor with default values
     *
     * @param User $sender
     * @param User $receiver
     *
     * @return Message
     */
    public static function withDefaultValues(User $sender, User $receiver) : self
    {
        $message  = new self();
        $chat = Chat::withDefaultValues($sender, $receiver);
        $message->setChatId($chat->getId());
        $message->setSender($sender);
        $message->setReceiver($receiver);
        $message->setSendAt(new DateTime());
        $message->setIsDeleted(false);
        $message->setIsRead(false);

        return $message;
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId(string $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getChatId()
    {
        return $this->chatId;
    }

    /**
     * @param string $chatId
     */
    public function setChatId(string $chatId): void
    {
        $this->chatId = $chatId;
    }

    /**
     * @return User
     */
    public function getSender()
    {
        return $this->sender;
    }

    /**
     * @param User $sender
     */
    public function setSender(User $sender): void
    {
        $this->sender = $sender;
    }

    /**
     * @return User
     */
    public function getReceiver()
    {
        return $this->receiver;
    }

    /**
     * @param User $receiver
     */
    public function setReceiver(User $receiver): void
    {
        $this->receiver = $receiver;
    }

    /**
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * @param string $text
     */
    public function setText(string $text): void
    {
        $this->text = $text;
    }

    /**
     * @return DateTime
     */
    public function getSendAt()
    {
        return $this->send_at;
    }

    /**
     * @param DateTime $send_at
     */
    public function setSendAt(DateTime $send_at): void
    {
        $this->send_at = $send_at;
    }

    /**
     * @return bool
     */
    public function getIsDeleted()
    {
        return $this->isDeleted;
    }

    /**
     * @param bool $isDeleted
     */
    public function setIsDeleted(bool $isDeleted): void
    {
        $this->isDeleted = $isDeleted;
    }

    /**
     * @return bool
     */
    public function getIsRead()
    {
        return $this->isRead;
    }

    /**
     * @param bool $isRead
     */
    public function setIsRead(bool $isRead): void
    {
        $this->isRead = $isRead;
    }
}