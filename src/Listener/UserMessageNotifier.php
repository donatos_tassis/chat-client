<?php

declare(strict_types=1);

namespace App\Listener;

use App\Entity\User;
use App\Service\FirestoreService;
use Doctrine\ORM\Event\LifecycleEventArgs;

class UserMessageNotifier
{
    /**
     * @var FirestoreService
     */
    private $firestore;

    /**
     * @param FirestoreService $firestore
     */
    public function __construct(FirestoreService $firestore)
    {
        $this->firestore = $firestore;
    }

    /**
     * @param User $user
     * @param LifecycleEventArgs $event
     */
    public function postUpdate(User $user, LifecycleEventArgs $event) : void
    {
        $this->firestore->updateUserNotifier($user);
    }
}