<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\User;
use App\Form\ChatType;
use App\Service\FirestoreService;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ChatController extends BaseController
{
    /**
     * @Route("/chat/{id}", name="app_chat_with")
     *
     * @param int $id -   the contact id of the contact the logged in user will chat with
     *
     * @return Response
     */
    public function chat(int $id)
    {
        /** @var User $receiver */
        $receiver = $this->getDoctrine()->getRepository(User::class)->find($id);

        if (!$receiver) {
            throw $this->createNotFoundException();
        }

        $form = $this->createForm(ChatType::class);

        return $this->render('chat/chat.html.twig', [
            'receiver' => $receiver,
            'chatId' => $this->getChat($id)->getId(),
            'chatForm' => $form->createView()
        ]);
    }

    /**
     * @Route("chat/{id}/messages", options={"expose" = true}, name="app_get_messages", methods={"GET"})
     *
     * @param FirestoreService $firestore
     * @param int $id - receiver (User) id wildcard
     *
     * @return JsonResponse
     */
    public function messages(FirestoreService $firestore, int $id)
    {
        $chat = $this->getChat($id);

        $messages = $firestore->getMessages($chat);
        $data = [
            'you' => $chat->getYou(),
            'otherUser' => $chat->getOtherUser(),
            'messages' => $messages,
        ];

        return $this->json($data);
    }

    /**
     * @Route("chat/{id}/messages-update", options={"expose" = true}, name="app_messages_update", methods={"POST"})
     *
     * @param FirestoreService $firestore
     * @param int $id - receiver (User) id wildcard
     *
     * @return JsonResponse
     */
    public function updateSeenMessages(FirestoreService $firestore, int $id)
    {
        $firestore->messagesUpdateRead($this->getChat($id));
        $this->getUser()->removeMessageFrom($id);
        $this->getDoctrine()->getManager()->persist($this->getUser());
        $this->getDoctrine()->getManager()->flush();

        return $this->json([], 204);
    }
}
