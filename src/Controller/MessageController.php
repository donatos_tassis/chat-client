<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\User;
use App\Form\ChatType;
use App\Service\FirestoreService;
use App\Service\NotificationService;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class MessageController extends BaseController
{
    /**
     * @Route("/chat/{id}/messages", options={"expose" = true}, name="app_submit_message", methods={"POST"})
     *
     * @param Request $request
     * @param int $id - receiver (User) id wildcard
     * @param FirestoreService $firestore
     * @param NotificationService $notification
     *
     * @return JsonResponse
     */
    public function submit(Request $request, int $id, FirestoreService $firestore, NotificationService $notification)
    {
        $data = json_decode($request->getContent(), true);

        /** @var User $receiver */
        $receiver = $this->getDoctrine()->getRepository(User::class)->find($id);

        if (!$receiver) {
            return new JsonResponse(['error' => 'Invalid action'], 404);
        }

        $message = $this->getMessage($id);
        $form = $this->createForm(ChatType::class, $message);
        $form->submit($data);

        if (!$form->isValid()) {
            $errors = $this->getErrorsFromForm($form);
            return new JsonResponse(['errors' => $errors], 400);
        }

        $message->setText($data['text']);
        $firestore->storeMessage($message);
        $notification->notifyReceiver($message);

        return new JsonResponse(null, 201);
    }
}
