<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\User;
use App\Firestore\Chat;
use App\Firestore\Message;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormInterface;

class BaseController extends AbstractController
{
    /**
     * Returns an associative array of validation errors
     *
     * @param FormInterface $form
     *
     * @return array|string
     */
    protected function getErrorsFromForm(FormInterface $form)
    {
        foreach ($form->getErrors() as $error) {
            // only supporting 1 error per field
            // and not supporting a "field" with errors, that has more
            // fields with errors below it
            return $error->getMessage();
        }

        $errors = array();
        foreach ($form->all() as $childForm) {
            if ($childForm instanceof FormInterface) {
                if ($childError = $this->getErrorsFromForm($childForm)) {
                    $errors[$childForm->getName()] = $childError;
                }
            }
        }

        return $errors;
    }

    /**
     * @param int $id
     *
     * @return Chat
     */
    protected function getChat(int $id)
    {
        /** @var User $receiver */
        $receiver = $this->getDoctrine()->getRepository(User::class)->find($id);
        /** @var User $sender */
        $sender = $this->getUser();

        return Chat::withDefaultValues($sender, $receiver);
    }

    /**
     * @param int $id
     *
     * @return Message
     */
    protected function getMessage(int $id)
    {
        /** @var User $sender */
        $sender = $this->getUser();
        /** @var User $receiver */
        $receiver = $this->getDoctrine()->getRepository(User::class)->find($id);

        return Message::withDefaultValues($sender, $receiver);
    }
}
