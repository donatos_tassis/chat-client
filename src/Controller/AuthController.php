<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\User;
use App\Entity\UserProfile;
use App\Form\UserLoginType;
use App\Form\UserRegistrationType;
use App\Service\FirestoreService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class AuthController extends AbstractController
{
    const PLAIN_PASSWORD = 'plainPassword';

    /**
     * @Route("/register", name="app_register")
     */
    public function register(Request $request, UserPasswordEncoderInterface $passwordEncoder)
    {
        $user = new User();
        $profile = new UserProfile();
        $form = $this->createForm(UserRegistrationType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $profile->setName((string)$form->get('profile')->get('name')->getData());
            $profile->setSurname((string)$form->get('profile')->get('surname')->getData());

            $user->setPassword(
                $passwordEncoder->encodePassword(
                    $user,
                    $form->get(self::PLAIN_PASSWORD)->getData()
                )
            );

            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();

            return $this->redirectToRoute('app_contacts');
        }

        return $this->render('auth/register.html.twig', [
            'registrationForm' => $form->createView(),
        ]);
    }

    /**
     * @Route("/login", name="app_login")
     */
    public function login(Request $request,AuthenticationUtils $authenticationUtils): Response
    {
         if ($this->getUser()) {
             return $this->redirectToRoute('app_contacts');
         }

        $form = $this->createForm(UserLoginType::class);
        $form->handleRequest($request);

        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();
        // last email entered by the user
        $lastEmail = $authenticationUtils->getLastUsername();

        return $this->render('auth/login.html.twig', [
            'loginForm' => $form->createView(),
            'last_email' => $lastEmail,
            'error' => $error
        ]);
    }

    /**
     * @Route("/logout", name="app_logout")
     */
    public function logout()
    {
        throw new \LogicException('This method can be blank - it will be intercepted by the logout key on your firewall.');
    }

    /**
     * @Route("/token", name="app_token_store", options={"expose" = true}, methods={"POST"})
     *
     * @param FirestoreService $firestore
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function token(FirestoreService $firestore, Request $request)
    {
        $data = json_decode($request->getContent(), true);
        $token = $data['token'];
        $firestore->storeToken($this->getUser(), $token);

        return new JsonResponse(null, 201);
    }
}
