<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class ContactsController extends AbstractController
{
    /**
     * @Route("/", name="app_contacts")
     */
    public function contacts()
    {
        $contacts = $this->getDoctrine()->getRepository(User::class)->contactInfo();

        return $this->render('contacts/contacts.html.twig', [
            'contacts' => $contacts
        ]);
    }
}
