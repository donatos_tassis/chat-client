<?php

declare(strict_types=1);

namespace App\DataFixtures;

use App\Entity\User;
use App\Entity\UserProfile;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserFixture extends BaseFixture
{
    private $userPasswordEncoder;

    public function __construct(UserPasswordEncoderInterface $userPasswordEncoder)
    {
        $this->userPasswordEncoder = $userPasswordEncoder;
    }

    protected function loadData(ObjectManager $manager): void
    {
        $this->createMany(30, function ($i) use ($manager) {
            $userProfile = new UserProfile();
            $userProfile->setAvatar(UserProfile::AVATAR_PREFIX_PATH.'avatar.jpg');
            $userProfile->setName($this->faker->firstName);
            $userProfile->setSurname($this->faker->lastName);
            $manager->persist($userProfile);

            $user = new User();
            $user->setEmail(sprintf('test%d@test.com', $i));
            $user->setPassword($this->userPasswordEncoder->encodePassword($user, 'test'));
            $user->setProfile($userProfile);

            return $user;
        });

        $manager->flush();
    }
}
