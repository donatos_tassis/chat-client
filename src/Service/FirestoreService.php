<?php

declare(strict_types=1);

namespace App\Service;

use App\Entity\User;
use App\Firestore\Chat;
use App\Firestore\Message;
use Google\Cloud\Firestore\CollectionReference;
use Google\Cloud\Firestore\DocumentReference;
use Google\Cloud\Firestore\DocumentSnapshot;
use Google\Cloud\Firestore\FirestoreClient;
use Google\Cloud\Firestore\QuerySnapshot;
use Kreait\Firebase\Firestore;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

class FirestoreService
{
    /** @var string */
    const USERS_COLLECTION = 'users';

    /** @var string  */
    const TOKENS_COLLECTION = 'tokens';

    /** @var string */
    const MESSAGES_COLLECTION = 'messages';

    /** @var string */
    const CHATS_COLLECTION = 'chats';

    /** @var Firestore */
    private $firestore;

    /** @var FirestoreClient */
    private $database;

    /** @var NormalizerInterface */
    private $normalizer;

    /**
     * @param Firestore $firestore
     * @param NormalizerInterface $normalizer
     */
    public function __construct(Firestore $firestore, NormalizerInterface $normalizer)
    {
        $this->firestore = $firestore;
        $this->database = $firestore->database();
        $this->normalizer = $normalizer;
    }

    /**
     * @return FirestoreClient
     */
    public function database()
    {
        return $this->database;
    }

    /**
     * Stores an array per user that indicates from which users there are unread messages
     *
     * @param User $user
     */
    public function updateUserNotifier(User $user) : void
    {
        $this->database
            ->collection(self::USERS_COLLECTION)
            ->document($user->getId())
            ->set(['messagesFrom' => $user->getMessagesFrom()]);
    }

    /**
     * @param User $user
     * @param string $token
     */
    public function storeToken(User $user, string $token) : void
    {
        $this->database
            ->collection(self::TOKENS_COLLECTION)
            ->document($user->getId())
            ->set(['token' => $token]);
    }

    /**
     * @param User $user
     *
     * @return string
     */
    public function getToken(User $user) : string
    {
        return $document = $this->database
            ->collection(self::TOKENS_COLLECTION)
            ->document($user->getId())
            ->snapshot()
            ->get('token');
    }

    /**
     * Stores the message to the firestore database
     *
     * @param Message $message
     */
    public function storeMessage(Message $message) : void
    {
        // normalize the message object
        $data = $this->normalizer->normalize($message);

        if(array_key_exists('id', $data))
            unset($data['id']);

        $this->database
            ->collection(self::CHATS_COLLECTION)
            ->document($message->getChatId())
            ->set([]);

        $this->database
            ->collection(self::CHATS_COLLECTION)
            ->document($message->getChatId())
            ->collection(self::MESSAGES_COLLECTION)
            ->add($data);
    }

    /**
     * Returns an array of messages between 2 users
     *
     * @param Chat $chat - the chat channel between the 2 users
     *
     * @return Message[]
     */
    public function getMessages(Chat $chat) : array
    {
        /** @var CollectionReference $ref */
        $messagesRef = $this->getMessagesReference($chat);

        $messages = [];
        $documents = $messagesRef->orderBy('sendAt', 'asc')->documents();
        /** @var DocumentSnapshot $document */
        foreach ($documents as $document) {
            $data = $document->data();
            $data['id'] = $document->id();

            /** @var Message $message */
            $message = $this->normalizer->denormalize($data, Message::class);
            $messages[] = $message;
        }

        return $messages;
    }

    /**
     * Updates messages isRead status where the receiver is you
     *
     * @param Chat $chat
     */
    public function messagesUpdateRead(Chat $chat) : void
    {
        /** @var CollectionReference $ref */
        $messagesRef = $this->getMessagesReference($chat);

        $documents = $messagesRef
            ->where('isRead', '=', false)
            ->where('receiver', '=', $chat->getYou()->getId())
            ->documents();

        /** @var DocumentSnapshot $document */
        foreach ($documents as $document) {
            $document->reference()->update([['path' => 'isRead', 'value' => true]]);
        }
    }

    /**
     * Gets an array of messages that haven't been read and are send more than 15minutes in the past
     *
     * @return array
     */
    public function unreadMessagesMoreThan15Minutes()
    {
        /** @var CollectionReference $chats */
        $chats = $this->database->collection(self::CHATS_COLLECTION)->documents();
        $time = new \DateTime(); // get current time
        $time->sub(new \DateInterval('PT15M')); // subtract 15 minutes from current time

        $unreadMessages = [];
        /** @var DocumentSnapshot $chat */
        foreach ($chats as $chat) {
            if ($chat->exists()) {
                /** @var QuerySnapshot $messages */
                $messages = $chat
                    ->reference()
                    ->collection(self::MESSAGES_COLLECTION)
                    ->where('isRead', '=', false)
                    ->where('sendAt', '<', $time->getTimestamp())
                    ->documents()
                    ->rows();

                /** @var DocumentReference $message */
                foreach ($messages as $message) {
                    $unreadMessages[] = $message->data();
                }
            }
        }

        return $unreadMessages;
    }

    /**
     * @param Chat $chat
     *
     * @return CollectionReference
     */
    private function getMessagesReference(Chat $chat) : CollectionReference
    {
        return $messagesRef = $this->database
            ->collection(self::CHATS_COLLECTION)
            ->document($chat->getId())
            ->collection(self::MESSAGES_COLLECTION);
    }
}