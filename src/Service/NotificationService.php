<?php

declare(strict_types=1);

namespace App\Service;

use App\Entity\User;
use App\Firestore\Message;
use Doctrine\ORM\EntityManagerInterface;
use Kreait\Firebase\Exception\Auth\UserNotFound;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class NotificationService
{
    /** @var string  */
    const FCM_URL = 'https://fcm.googleapis.com/fcm/send';

    /**
     * @var FirestoreService
     */
    private $firestore;

    /**
     * @var HttpClientInterface
     */
    private $client;

    /**
     * @var EntityManagerInterface
     */
    private $manager;

    /**
     * @var string
     */
    private $firebaseServerKey;

    /**
     * @param FirestoreService $firestore
     * @param HttpClientInterface $client
     * @param EntityManagerInterface $manager
     * @param string $firebaseServerKey
     */
    public function __construct(
        FirestoreService $firestore,
        HttpClientInterface $client,
        EntityManagerInterface $manager,
        string $firebaseServerKey
    ) {
        $this->firestore = $firestore;
        $this->client = $client;
        $this->manager = $manager;
        $this->firebaseServerKey = $firebaseServerKey;
    }

    /**
     * @param Message $message
     */
    public function notifyReceiver(Message $message)
    {
        $token = $this->firestore->getToken($message->getReceiver());

        if (!$token) {
            throw new UserNotFound(); // ToDo: this needs to be handled so there is no User without a token
        }

        $this->updateUserEntity($message);
        $this->client->request('POST', self::FCM_URL, [
            'headers' => [
                'Authorization' => 'key='.$this->firebaseServerKey
            ],
            'json' => [
                'to' => $token,
                'notification' => [
                    'title' => 'New Message from '. $message->getReceiver()->getProfile()->getName(),
                    'body' => $message->getText()
                ]
            ]
        ]);
    }

    /**
     * @param Message $message
     */
    private function updateUserEntity(Message $message)
    {
        /** @var User $receiver */
        $receiver = $message->getReceiver();
        $receiver->addMessagesFrom($message->getSender()->getId());
        $this->manager->persist($receiver);
        $this->manager->flush();
    }
}
