# Chat-Client

Symfony 5.1 project

## Requirements

- Firebase project setup - on Cloud Firestore
    ##### https://firebase.google.com/
- SendGrid SMTP mail server -with one Sender email verified
    ##### https://sendgrid.com/


## Setup

### Docker

On Docker there is a missing php extension  ``` grpc ```

to install it follow those steps:

##### ```cd ~/git/devenviroment```
##### ```bin/console attach symfony_sandbox_httpd```
##### ```pecl install grpc```
##### ```docker-php-ext-enable grpc```

and restart apache service with:

##### ```service apache2 restart```

### Configure PHP dependencies
##### ```composer install```

Replace SendGrid and Firebase credentials on .env with your own

### Database configuration and fixtures
##### ```bin/console doctrine:database:create```
##### ```bin/console doctrine:migrations:migrate```
##### ```bin/console doctrine:fixtures:load```

### Configure JS dependencies
##### ```npm install```
##### ```npm run watch```

You can login now in the app with:
- email : ```test0@test.com``` or ```test1@test.com``` up to test29
- password : ```test``` the same for all accounts

### Cron Jobs to be configured
Every 15 minutes sends email to users with list of unread messages

```bin/console email:users```