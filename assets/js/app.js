import '../css/app.less';

import $ from 'jquery';
import 'bootstrap';
import { getToken } from './components/firebase';


$('.dropdown-toggle').dropdown();

getToken();