import '../css/components/contacts.less';

import { firestore } from './components/firebase';
import $ from 'jquery';

const Contacts = () => {

    const id = $('#user').attr('data-id');

    firestore.collection('users').doc(id).onSnapshot((users) => {
        if (typeof users.data() !== 'undefined') {
            Object.values(users.data().messagesFrom).forEach((user) => {
                if (!document.getElementById('alert-'+user)) {
                    const contact = document.querySelector('#contact-'+user);
                    const alert = document.createElement('article');
                    alert.textContent = 'New message';
                    alert.classList.add('alert-danger');
                    alert.classList.add('alert-message');
                    alert.id = 'alert-'+user;
                    contact.appendChild(alert);
                }
            });
        }
    });
};

export default Contacts();