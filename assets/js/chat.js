import '../css/components/chat.less';

import $ from 'jquery';
import axios from 'axios';
import Message from './components/message';
import { onMessage, messagesListener } from './components/firebase';
import { scrollListener, scrollToBottom, isBottom } from './components/scrollHelper';
import { generateUrl } from './components/router';

const Chat = () => {
    const messagesEl = $('#message-list');
    const id = $('#chat_form').attr('data-id');
    const chatID = $('#chat-room').attr('data-chatid');
    let you;
    let otherUser;

    const submitMessage = () => {
        $('#chat_form').on('submit', (event) => {
            event.preventDefault();

            let form = $(event.currentTarget);
            let data = {};
            $.each(form.serializeArray(), (key, value) => {
                data[value.name] = value.value;
            });
            $('#text').val('');

            let url = generateUrl('app_submit_message', { id : id });
            let wasAtBottom = isBottom(messagesEl);
            axios.post(url, data).then(() => {
                scrollToBottom(messagesEl, wasAtBottom);
            });
        });
    };

    const getMessages = () => {
        let url = generateUrl('app_get_messages', { id : id });
        axios.get(url)
            .then(response => {
                you = response.data.you;
                otherUser = response.data.otherUser;
                messagesEl.empty();
                updateMessagesList(response.data.messages , true);
                scrollToBottom(messagesEl, true);

            })
            .catch(error => {
                console.log(error);
            });
    };

    const loadMessages = () => {
        localStorage.setItem('scroll', 0);
        getMessages();
    };

    const updateMessagesList = (messages, firstLoad) => {
        let unreadMessages = [];
        messages.forEach((message) => {
            if (!message.isRead && message.receiver === you.id)
                unreadMessages.push(message);

            if (firstLoad)
                messagesEl.append(Message(message, you, otherUser));
        });

        if (unreadMessages.length || !firstLoad) {
            let url = generateUrl('app_messages_update', { id : id });
            axios.post(url); // ToDo: it updates the status of the messages even if the tab is on the background
        }
    };

    messagesListener(chatID, (type, msg, msgID) => {
        messagesEl.scrollTop(localStorage.getItem('scroll'));
        let newMessage = Message(msg, you, otherUser, msgID);

        if (type === 'added' && !document.getElementById(msgID)) {
            messagesEl.append(newMessage);
            // update messages from delivered to seen once the new message is been added and detected
            updateMessagesList([], false);
        }

        if (type === 'modified' && document.getElementById(msgID)) {
            // replace old div element with a new one that contains the modified values
            let oldMessage = document.getElementById(msgID);
            oldMessage.parentNode.replaceChild(newMessage, oldMessage);
        }
    });

    scrollListener(messagesEl);
    loadMessages();
    submitMessage();
    let wasAtBottom = isBottom(messagesEl);
    onMessage(() => {
        scrollToBottom(messagesEl, wasAtBottom);
    });
};

export default Chat();