import routes from '../../../public/js/fos_js_routes';
import Routing from '../../../vendor/friendsofsymfony/jsrouting-bundle/Resources/public/js/router.min';

const setRoutes = () => {
    Routing.setRoutingData(routes);
};

export const generateUrl = (url, params) => {
    setRoutes();
    return Routing.generate(url, params);
};