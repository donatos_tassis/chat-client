import firebase from 'firebase/app';
import 'firebase/firestore';
import 'firebase/messaging';
import axios from 'axios';
import { generateUrl } from './router';

const firebaseApp = firebase.initializeApp({
        apiKey: process.env.FIREBASE_API_KEY,
        authDomain: process.env.FIREBASE_AUTH_DOMAIN,
        projectId: process.env.FIREBASE_PROJECT_ID,
        storageBucket: process.env.FIREBASE_STORAGE_BUCKET,
        messagingSenderId: process.env.FIREBASE_MESSAGING_SENDER_ID,
        appId: process.env.FIREBASE_APP_ID,
        measurementId: process.env.FIREBASE_MEASUREMENT_ID
});

export const firestore = firebaseApp.firestore();
export const messaging = firebaseApp.messaging();


const sendTokenToServer = (token) => {
    let url = generateUrl('app_token_store');
    axios.post(url, {token: token})
        .then(() => {});
};

export const getToken = () => {
    Notification.requestPermission().then( () => {
        messaging.getToken({
            vapidKey: process.env.FIREBASE_VAPID_KEY,
        })
            .then((currentToken) => {
                if (currentToken) {
                    sendTokenToServer(currentToken);
                } else {
                    console.log('No Instance ID token available. Request permission to generate one.');
                }
            })
            .catch((err) => {
                console.log('An error occurred while retrieving token. ', err);
            });
    });
};

export const onMessage = (fn) => {
    messaging.onMessage(fn)
};

export const messagesListener = (chatId, onChange) => {
    let initState = true;
    firestore
        .collection('chats')
        .doc(chatId)
        .collection('messages')
        .onSnapshot((snapshot) => {
            if (!initState) {
                snapshot.docChanges().forEach((change) => {
                    onChange(change.type, change.doc.data(), change.doc.id);
                });
            }

            initState = false;
        });
};
