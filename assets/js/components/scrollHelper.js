export const scrollListener = (element) => {
    element.scroll( () => {
        let scrollPos = element.scrollTop();
        localStorage.setItem('scroll', scrollPos);
        isBottom(element);
    });
};

export const scrollToBottom = (element, wasAtBottom = true) => {
    if (wasAtBottom)
        element.scrollTop(element.prop('scrollHeight'));
};

export const isBottom = (element) => {
    return element.prop('scrollHeight') - element.scrollTop() === element.prop('clientHeight');
};