const Message = (message, you, otherUser, msgID = null) => {
    const messageDiv = document.createElement('div');
    const messageClass = message.sender === you.id ? 'yours' : 'message';
    messageDiv.classList.add(messageClass);
    messageDiv.id = message.id ? message.id : msgID;
    const avatarDiv = document.createElement('div');
    avatarDiv.classList.add('avatar');
    const avatarImg = document.createElement('img');
    avatarImg.src = '../' + you.profile.avatar;
    const senderName = document.createElement('article');
    let user = message.sender === you.id ? you : otherUser;
    senderName.textContent = user.profile.name + ' ' + user.profile.surname;
    avatarDiv.appendChild(avatarImg);
    avatarDiv.appendChild(senderName);
    const messageText = document.createElement('p');
    messageText.textContent = message.text;
    messageText.classList.add('message-text');
    const time = document.createElement('span');
    let dateTime = new Date(message.sendAt * 1000);
    time.textContent = dateTime.toLocaleDateString() + ' ' + dateTime.toLocaleTimeString();
    time.classList.add('time');
    const isRead = document.createElement('span');
    isRead.textContent = message.isRead ? 'Seen' : 'Delivered';
    messageDiv.appendChild(avatarDiv);
    messageDiv.appendChild(messageText);
    messageDiv.appendChild(time);
    messageDiv.appendChild(isRead);

    return messageDiv;
};

export default Message;