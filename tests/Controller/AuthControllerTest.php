<?php

declare(strict_types=1);

namespace App\Tests\Controller;

use App\Entity\User;
use App\Entity\UserProfile;
use Doctrine\ORM\EntityManager;
use Liip\FunctionalTestBundle\Test\WebTestCase;
use Symfony\Bundle\FrameworkBundle\Client;
use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Component\Validator\ConstraintViolationList;

class AuthControllerTest extends WebTestCase
{
    /** @var EntityManager */
    private $manager;

    /** @var Client */
    private $client;

    protected function setUp() : void
    {
        $this->client = $this->makeClient();
        $kernel = self::bootKernel();
        $this->manager = $kernel->getContainer()->get('doctrine')->getManager();

        parent::setUp();
    }

    public function testRegisterTemplateLoads()
    {
        $this->client->request('GET', '/register');
        $this->assertStatusCode(200, $this->client);
    }

    public function testValidationErrorsOnEmptyFormSubmit()
    {
        $crawler = $this->client->request('GET', '/register');
        $this->assertStatusCode(200, $this->client);

        $form = $crawler->selectButton('Register')->form();
        $this->client->submit($form);
        $this->assertValidationErrors([
            'children[email].data',
            'children[plainPassword].data',
            'children[profile].children[name].data',
            'children[profile].children[surname].data'
        ], $this->client->getContainer());
    }

    public function testValidationSuccessfulWithValidFormSubmit()
    {
        $crawler = $this->client->request('GET', '/register');
        $form = $crawler->selectButton('Register')->form();

        $formData = [
            'user_registration[email]' => 'user@test.com',
            'user_registration[profile][name]' => 'name',
            'user_registration[profile][surname]' => 'surname',
            'user_registration[plainPassword][first]' => 'password',
            'user_registration[plainPassword][second]' => 'password',
        ];

        // Submit the form with valid data
        $form->setValues($formData);
        $this->client->submit($form);
        $this->assertValidationErrors([], $this->client->getContainer());
        $this->assertStatusCode(302, $this->client);

        // Check if the submitted form inserted a record into the database with the given data
        /** @var User $user */
        $user = $this->manager->getRepository(User::class)->findOneBy(['email' => 'user@test.com']);
        $this->assertNotNull($user);
        $this->assertInstanceOf(User::class, $user);
        $this->assertSame('user@test.com', $user->getEmail());
        $this->assertInstanceOf(UserProfile::class, $user->getProfile());
        $this->assertSame('name', $user->getProfile()->getName());
        $this->assertSame('surname', $user->getProfile()->getSurname());

        // Re-submit the form with the same data to check for existing user validation error
        $this->client->submit($form);
        $this->assertValidationErrors(['data.email'], $this->client->getContainer());
        /** @var ConstraintViolationList $violations */
        $violations = $this->client->getContainer()->get('liip_functional_test.validator')->getLastErrors();
        $this->assertCount(1, $violations);
        /** @var ConstraintViolation $violation */
        $violation = $violations->getIterator()->current();
        $this->assertSame('There is already an account with this email', $violation->getMessage());
    }

    protected function tearDown(): void
    {
        parent::tearDown();

        // close entity manager to avoid memory leaks
        $this->manager->close();
        $this->manager = null;
    }
}
